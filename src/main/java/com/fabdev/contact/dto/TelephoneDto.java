package com.fabdev.contact.dto;

import com.fabdev.contact.entities.ETypePhone;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TelephoneDto {
    @Schema(description = "valeur probable", example = "BUREAU,DOMICILE,PRINCIPAL,FAX_BUREAU,FAX_DOMICILE,BIPEUR,AUTRE")
    private String typePhone;

    private String phone;
}
