package com.fabdev.contact.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ContactDto {

    private String firstName;

    private String lastName;

    private String notes;

    private String email;

    private List<Long> groupeIds;

    private List<TelephoneDto> telephoneDtos;
}
