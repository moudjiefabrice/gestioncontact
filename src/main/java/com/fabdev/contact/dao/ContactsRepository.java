package com.fabdev.contact.dao;

import com.fabdev.contact.entities.Contacts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactsRepository extends JpaRepository<Contacts,Long> {

}
