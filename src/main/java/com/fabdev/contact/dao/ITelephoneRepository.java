package com.fabdev.contact.dao;

import com.fabdev.contact.entities.Telephone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ITelephoneRepository extends JpaRepository<Telephone,Long> {
}
