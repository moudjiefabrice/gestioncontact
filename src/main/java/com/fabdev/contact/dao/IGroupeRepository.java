package com.fabdev.contact.dao;

import com.fabdev.contact.entities.Groupes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IGroupeRepository extends JpaRepository<Groupes,Long> {
}
