package com.fabdev.contact.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Telephone {

    @Schema(description = "identifiant unique du numero de téléphone", example = "1", required = true, accessMode = Schema.AccessMode.READ_ONLY)
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long Id;

    private ETypePhone typePhone;

    private String phone;

    @ManyToOne
    @JsonIgnore
    private Contacts contacts;
}
