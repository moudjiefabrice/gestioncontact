package com.fabdev.contact.entities;

public enum ETypePhone {
    BUREAU,DOMICILE,PRINCIPAL,FAX_BUREAU,FAX_DOMICILE,BIPEUR,AUTRE
}
