package com.fabdev.contact.entities;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Contacts {

    @Schema(description = "identifiant unique du contact", example = "1", required = true, accessMode = Schema.AccessMode.READ_ONLY)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long Id;

    private String firstName;

    private String lastName;

    private String email;

    @Column(columnDefinition ="TEXT")
    private String notes;

    @ManyToMany
    private List<Groupes> groupesList = new ArrayList<>();

    @OneToMany(mappedBy = "contacts",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Telephone> telephoneList = new ArrayList<>();




}
