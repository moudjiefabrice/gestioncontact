package com.fabdev.contact.service;

import com.fabdev.contact.entities.Contacts;
import com.fabdev.contact.entities.Groupes;
import com.fabdev.contact.entities.Telephone;


import java.util.List;

public interface ContactService {

    Contacts getOneContact(Long contactId);

    Groupes getOneGroupe(Long groupeId);

    Telephone getOnePhone(Long phoneId);

    Contacts saveContact(Contacts contacts);

    Contacts saveContact(Contacts contacts, List<Long> groupeIds);

    Telephone savePhone(Telephone telephone, Long contactId);

    Groupes saveGroupe(Groupes groupes);

    List<Contacts> getAllContacts();

    List<Groupes> getAllGroupe();

    Contacts updateContact(Contacts contacts, Long id);

    void deleteContact(Long contactId);

    void deleteGroupe(Long groupeId);

    void deletePhone(Long phoneId);
}
