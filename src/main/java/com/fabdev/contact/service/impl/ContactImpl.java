package com.fabdev.contact.service.impl;

import com.fabdev.contact.dao.ContactsRepository;
import com.fabdev.contact.dao.IGroupeRepository;
import com.fabdev.contact.dao.ITelephoneRepository;
import com.fabdev.contact.entities.Contacts;
import com.fabdev.contact.entities.Groupes;
import com.fabdev.contact.entities.Telephone;
import com.fabdev.contact.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ContactImpl implements ContactService {

    @Autowired
    ContactsRepository contactsRepo;

    @Autowired
    IGroupeRepository groupeRepo;

    @Autowired
    ITelephoneRepository telephoneRepo;

    @Autowired
    private ResourceBundleMessageSource messageSource;

    @Override
    public Contacts getOneContact(Long contactId) {
        return contactsRepo.findById(contactId).orElseThrow(() -> new ResourceNotFoundException(
                messageSource.getMessage("messages.contact-not-found", null, LocaleContextHolder.getLocale())));
    }

    @Override
    public Groupes getOneGroupe(Long groupeId) {
        return groupeRepo.findById(groupeId).orElseThrow(() -> new ResourceNotFoundException(
                messageSource.getMessage("messages.groupe-not-found", null, LocaleContextHolder.getLocale())));
    }

    @Override
    public Telephone getOnePhone(Long phoneId) {
        return telephoneRepo.findById(phoneId).orElseThrow(() -> new ResourceNotFoundException(
                messageSource.getMessage("messages.phone-not-found", null, LocaleContextHolder.getLocale())));
    }

    @Override
    public Contacts saveContact(Contacts contacts) {
        return contactsRepo.save(contacts);
    }

    @Override
    public Contacts saveContact(Contacts contacts, List<Long> groupeIds) {
        List<Groupes> groupes = groupeRepo.findAllById(groupeIds);
        contacts.setGroupesList(groupes);
        return contactsRepo.save(contacts);
    }

    @Override
    public Telephone savePhone(Telephone telephone, Long contactId) {
        Contacts contact = getOneContact(contactId);
        telephone.setContacts(contact);
        return telephoneRepo.save(telephone);
    }

    @Override
    public Groupes saveGroupe(Groupes groupes) {
        return groupeRepo.save(groupes);
    }

    @Override
    public List<Contacts> getAllContacts() {
        return contactsRepo.findAll();
    }

    @Override
    public List<Groupes> getAllGroupe() {
        return groupeRepo.findAll();
    }

    @Override
    public Contacts updateContact(Contacts contacts, Long id) {
        Contacts contactfind = getOneContact(id);
        contactfind.setEmail(contacts.getEmail());
        contactfind.setNotes(contacts.getNotes());
        contactfind.setFirstName(contacts.getFirstName());
        contactfind.setLastName(contacts.getLastName());
        return contactsRepo.save(contactfind);
    }

    @Override
    public void deleteContact(Long contactId) {
     Contacts contact = getOneContact(contactId);
     contactsRepo.delete(contact);
    }

    @Override
    public void deleteGroupe(Long groupeId) {
    Groupes groupe = getOneGroupe(groupeId);
    groupeRepo.delete(groupe);
    }

    @Override
    public void deletePhone(Long phoneId) {
       Telephone telephone = getOnePhone(phoneId);
       telephoneRepo.delete(telephone);
    }
}
