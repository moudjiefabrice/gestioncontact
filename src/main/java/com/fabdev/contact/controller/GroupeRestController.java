package com.fabdev.contact.controller;

import com.fabdev.contact.dto.GroupeDto;
import com.fabdev.contact.dto.MessageResponseDto;
import com.fabdev.contact.entities.Groupes;
import com.fabdev.contact.service.ContactService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "groupe")
@RequestMapping("/api/v1.0/groupe")
@Slf4j
public class GroupeRestController {

    @Autowired
    ContactService contactService;

    @Autowired
    private ResourceBundleMessageSource messageSource;

    @Operation(summary = "Enregistrement d'un groupe", tags = "groupe", responses = {
            @ApiResponse(responseCode = "200", description = "groupe enregistré avec succès", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Groupes.class)))),
            @ApiResponse(responseCode = "400", description = "Mauvais format de données", content = @Content(mediaType = "Application/Json"))})
    @PostMapping()
    public ResponseEntity<Groupes> saveGroupe(@RequestBody GroupeDto groupeDto) {
      Groupes groupe = new Groupes(groupeDto.getName());
      Groupes save = contactService.saveGroupe(groupe);
      return  ResponseEntity.status(HttpStatus.CREATED).body(save);
    }

    @Operation(summary = "Recupérer la liste des groupes", tags = "groupe", responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Groupes.class)))),
            @ApiResponse(responseCode = "404", description = "Liste    groupes.add(3L); des contacts non disponible", content = @Content(mediaType = "Application/Json"))})
    @GetMapping()
    public ResponseEntity<List<Groupes>> getListContact() {
        List<Groupes> groupes = contactService.getAllGroupe();
        return ResponseEntity.ok(groupes);
    }

    @Operation(summary = "Supprimer un groupe", tags = "groupe", responses = {
            @ApiResponse(responseCode = "200", description = "groupe supprimé avec succès", content = @Content(mediaType = "Application/Json")),
            @ApiResponse(responseCode = "404", description = "groupe non existant", content = @Content(mediaType = "Application/Json"))})
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteContact(@PathVariable Long id) {
        contactService.deleteGroupe(id);
        return ResponseEntity.ok(new MessageResponseDto(
                messageSource.getMessage("request_successful-delete", null, LocaleContextHolder.getLocale())));
    }
}
