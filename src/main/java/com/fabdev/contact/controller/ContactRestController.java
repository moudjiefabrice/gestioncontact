package com.fabdev.contact.controller;

import com.fabdev.contact.dto.ContactDto;
import com.fabdev.contact.dto.MessageResponseDto;
import com.fabdev.contact.dto.TelephoneDto;
import com.fabdev.contact.entities.Contacts;
import com.fabdev.contact.entities.ETypePhone;
import com.fabdev.contact.entities.Telephone;
import com.fabdev.contact.service.ContactService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@OpenAPIDefinition(info = @Info(title = "API Contact V1.0", description = "Documentation de l'API", version = "1.0"))
@RestController
@Tag(name = "contact")
@RequestMapping("/api/v1.0/contact")
@Slf4j
public class ContactRestController {

    @Autowired
    ContactService contactService;

    @Autowired
    private ResourceBundleMessageSource messageSource;

    @Autowired
    private ModelMapper modelMapper;

    @Operation(summary = "Enregistrement d'un contact", tags = "contact", responses = {
            @ApiResponse(responseCode = "201", description = "Contact enregistré avec succès", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Contacts.class)))),
            @ApiResponse(responseCode = "400", description = "Mauvais format de données", content = @Content(mediaType = "Application/Json")),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur", content = @Content(mediaType = "Application/Json"))})
    @PostMapping("")
    public ResponseEntity<?> saveContact(@RequestBody ContactDto contactDto) {
        if (contactDto.getTelephoneDtos().size() == 0) {
            return new ResponseEntity<>(new MessageResponseDto(HttpStatus.BAD_REQUEST,
                    messageSource.getMessage("error-validation-phone", null, LocaleContextHolder.getLocale())), HttpStatus.BAD_REQUEST);
        }
        List<Telephone> phoneSave = new ArrayList<>();
        Contacts contact = modelMapper.map(contactDto, Contacts.class);
        Contacts contactSave = null;
        if (contactDto.getGroupeIds().isEmpty()) {
            contactSave = contactService.saveContact(contact);
        } else {
            contactSave = contactService.saveContact(contact, contactDto.getGroupeIds());
        }
        for (TelephoneDto phoneDto : contactDto.getTelephoneDtos()) {
            Telephone telephone = new Telephone();
            telephone.setPhone(phoneDto.getPhone());
            switch (phoneDto.getTypePhone()) {
                case "BUREAU":
                    telephone.setTypePhone(ETypePhone.BUREAU);
                    break;
                case "DOMICILE":
                    telephone.setTypePhone(ETypePhone.DOMICILE);
                    break;
                case "PRINCIPAL":
                    telephone.setTypePhone(ETypePhone.PRINCIPAL);
                    break;
                case "FAX_BUREAU":
                    telephone.setTypePhone(ETypePhone.FAX_BUREAU);
                    break;
                case "FAX_DOMICILE":
                    telephone.setTypePhone(ETypePhone.FAX_DOMICILE);
                    break;
                case "BIPEUR":
                    telephone.setTypePhone(ETypePhone.BIPEUR);
                    break;
                default:
                    telephone.setTypePhone(ETypePhone.AUTRE);
            }
            Telephone telephonesave = contactService.savePhone(telephone, contactSave.getId());
            phoneSave.add(telephonesave);
        }
        contactSave.setTelephoneList(phoneSave);

        return new ResponseEntity<>(contactSave, HttpStatus.CREATED);
    }

    @Operation(summary = "Recupérer la liste des contacts", tags = "contact", responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Contacts.class)))),
            @ApiResponse(responseCode = "404", description = "Liste des contacts non disponible", content = @Content(mediaType = "Application/Json"))})
    @GetMapping("")
    public ResponseEntity<List<Contacts>> getListContact() {
        List<Contacts> contacts = contactService.getAllContacts();
        return ResponseEntity.ok(contacts);
    }

    @Operation(summary = "Supprimer un contact", tags = "contact", responses = {
            @ApiResponse(responseCode = "200", description = "contact supprimé avec succès", content = @Content(mediaType = "Application/Json")),
            @ApiResponse(responseCode = "404", description = "Contact non existant", content = @Content(mediaType = "Application/Json"))})
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteContact(@PathVariable Long id) {
        contactService.deleteContact(id);
        return ResponseEntity.ok(new MessageResponseDto(
                messageSource.getMessage("request_successful-delete", null, LocaleContextHolder.getLocale())));
    }


    @Operation(summary = "Modification des informations de base d'un contact", tags = "contact", responses = {
            @ApiResponse(responseCode = "201", description = "Contact modiffié avec succès", content = @Content(mediaType = "Application/Json", array = @ArraySchema(schema = @Schema(implementation = Contacts.class)))),
            @ApiResponse(responseCode = "400", description = "Mauvais format de données", content = @Content(mediaType = "Application/Json")),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur", content = @Content(mediaType = "Application/Json"))})
    @PutMapping("/{id}")
    public ResponseEntity<?> updateContact(@RequestBody ContactDto contactDto, @PathVariable Long id) {
        Contacts contact = modelMapper.map(contactDto, Contacts.class);

        Contacts update = contactService.updateContact(contact, id);

        return new ResponseEntity<>(update, HttpStatus.CREATED);
    }

}