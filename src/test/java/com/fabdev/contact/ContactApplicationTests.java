package com.fabdev.contact;

import com.fabdev.contact.dto.ContactDto;
import com.fabdev.contact.dto.GroupeDto;
import com.fabdev.contact.dto.TelephoneDto;
import com.fabdev.contact.entities.Contacts;
import com.fabdev.contact.service.ContactService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
class ContactApplicationTests {

    @Autowired
    private MockMvc mockMvc;


    @Test
    @Order(1)
    public void createContact() throws Exception {
        List<TelephoneDto> telephoneDtoList = new ArrayList<>();
        telephoneDtoList.add(new TelephoneDto("BUREAU", "+237654532145"));
        telephoneDtoList.add(new TelephoneDto("DOMICILE", "+237690356436"));
        List<Long> groupes = new ArrayList<>();
        groupes.add(1L);
        ContactDto contactDto = new ContactDto("test", "test", "voici un test", "test@gmail.com", groupes, telephoneDtoList);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1.0/contact")
                        .content(asJsonString(contactDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    @Order(2)
    public void getAllContact() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/v1.0/contact")
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    @Order(3)
    public void updateContact() throws Exception {
        List<TelephoneDto> telephoneDtoList = new ArrayList<>();
        telephoneDtoList.add(new TelephoneDto("BUREAU", "+237654532145"));
        telephoneDtoList.add(new TelephoneDto("DOMICILE", "+237690356436"));
        List<Long> groupes = new ArrayList<>();
        groupes.add(1L);
        groupes.add(3L);
        ContactDto contactDto = new ContactDto("test update", "test update", "voici la modification des variable de base", "test@gmail.com", groupes, telephoneDtoList);
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/v1.0/contact/{id}", 1L)
                        .content(asJsonString(contactDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @Test
    @Order(4)
    public void deleteEmployeeAPI() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1.0/contact/{id}", 1L))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
