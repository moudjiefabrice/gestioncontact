FROM openjdk:11
ADD target/contact.jar contact.jar
ENTRYPOINT ["java", "-jar","contact.jar"]
EXPOSE 8080

