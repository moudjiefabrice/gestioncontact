Sur Linux :
Voici comment déployer le projet grâce à docker afin de faire les tests 
se mettre en mode super administrateur
```bash
sudo su
```
Ensuite taper juste la commande :
```bash
 bash contactapp.sh 
```

1. lien pour l'api

    * [`backend_api`](http://localhost:8080)
   
2. lien pour la documentation swagger

* [`doc_api`](http://localhost:8080/swagger-ui/index.html?configUrl=/api-docs/swagger-config#/)